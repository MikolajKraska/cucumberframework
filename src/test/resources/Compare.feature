Feature: Open morele.net page
	User should be able to open and navigate on page correctly

	Scenario: Comparison of the cheapest and the most expensive device
		Given User navigates to morele.net website
		And User checks the operation of grouping on the page
		When User selects the cheapest and the most expensive device
		Then User compare two devices

	Scenario: Comparison of the most commented and the most popular devices
		Given User navigates to morele.net website
		When User selects the most commented and the most popular devices
		Then User compare two devices