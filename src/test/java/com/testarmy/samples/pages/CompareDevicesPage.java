package com.testarmy.samples.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.logging.Level;

public class CompareDevicesPage extends BasePage {

    public CompareDevicesPage(WebDriver driver) {
        super(driver);
    }


    public static By FIRST_PRODUCER_NAME = By.cssSelector("#compare-container > div:nth-child(3) > div:nth-child(7) > div:nth-child(2)");
    public static By SECOND_PRODUCER_NAME = By.cssSelector("#compare-container > div:nth-child(3) > div:nth-child(7) > div:nth-child(3)");
    public static By ADD_TO_CART = By.cssSelector("#fixed > div.row.price-row > div:nth-child(2) > div > a");

    public static By FIRST_PRODUCT_PRICE = By.cssSelector("#fixed > div.row.price-row > div:nth-child(2)");
    public static By SECOND_PRODUCT_PRICE = By.cssSelector("#fixed > div.row.price-row > div:nth-child(3)");


    public boolean isAddToCartClickable(){
        return isElementClickable(ADD_TO_CART);
    }
    public String[] getProductDetails(){
        String[] listOfStrings = new String[2];
        listOfStrings[0] = saveText(FIRST_PRODUCER_NAME);
        listOfStrings[1] = saveText(SECOND_PRODUCER_NAME);
        return listOfStrings;

    }
    public void compareDevicesValues(By element,By element2){
        String valueOfFirstProduct = driver.findElement(element).getText();
        String valueOfSecondProduct = driver.findElement(element2).getText();

        double firstProductPrice = Double.parseDouble(valueOfFirstProduct.split("z")[0].replace(",",".").replace(" ",""));
        double secondProductPrice = Double.parseDouble(valueOfSecondProduct.split("z")[0].replace(",",".").replace(" ",""));
        Assert.assertTrue("Second price should be bigger",firstProductPrice<secondProductPrice);
    }
}
