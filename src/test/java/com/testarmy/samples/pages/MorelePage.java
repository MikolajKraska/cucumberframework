package com.testarmy.samples.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MorelePage extends BasePage {

    // Dlaczego mamy ten kod tutaj
    public MorelePage(WebDriver driver){
        super(driver);
    }

    public String DROPDOWN_LIST = "//button[@class='md-button form-control']";
    public String AGD = "//option[@value='4']";
    public String TELEFONY = "[value='5']";
    public String LAPTOPY = "[class='dropdown-native-select form-control']>[value='8']";


    final By MAIN_PAGE_SEARCH = By.cssSelector("#header  div.h-quick-search > form > div > input");
    final By DROPDOWN_FIRST_ELEMENT = By.cssSelector("#header div.ma-result.ma-show-result.ma-show-result-animation > div:nth-child(1)");

    public static By COOKIE = By.xpath("//button[@class='btn btn-blue btn-blue-outline btn-md close-cookie-box']");

    public void checkDropdownElements(){
        clickOnXpathElement(DROPDOWN_LIST);
        checkTextElementByXpath(AGD, "AGD");
        checkTextElementByCss(TELEFONY, "Telefony");
        checkTextElementByCss(LAPTOPY, "Laptopy");
        clickOnXpathElement(DROPDOWN_LIST);
        clickOnCssElement(TELEFONY);
    }

    public void searchText(String searchingText){

        sendText(MAIN_PAGE_SEARCH, searchingText);
        clickOnElement(DROPDOWN_FIRST_ELEMENT);
    }

}
