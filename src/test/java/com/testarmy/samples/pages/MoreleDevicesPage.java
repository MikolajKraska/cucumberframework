package com.testarmy.samples.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MoreleDevicesPage extends BasePage{

    public MoreleDevicesPage(WebDriver driver) { super(driver); }

    public static By SORT_PRICE_FROM_LOW = By.cssSelector("#category div.md-list > ul > li:nth-child(2)");
    public static By SORT_PRICE_FROM_HIGH = By.cssSelector("#category div.md-list > ul > li:nth-child(3)");

    public static By MOST_POPULAR = By.cssSelector("#category div.md-list > ul > li:nth-child(4)");
    public static By COMMENTS_AMOUNT = By.cssSelector("#category div.md-list > ul > li:nth-child(6)");

    final By FILTER_PRICE_FROM = By.cssSelector("#category div.input-range-bottom.has-all-controls > div.input-range-inputs > div:nth-child(1) > input");
    final By FILTER_PRICE_TO = By.cssSelector("#category  div.input-range-bottom.has-all-controls > div.input-range-inputs > div:nth-child(2) > input");
    final By FILTER_BUTTON = By.cssSelector("#category  div.input-range-button > button");

    final By SORTING_BUTTON = By.cssSelector("#category  div > div.md-top > button");
    final By FIRST_PRODUCT = By.cssSelector("#category div:nth-child(1) > div > div.cat-product-left > div > button");
    final By SECOND_PRODUCT = By.cssSelector("#category div:nth-child(1) > div > div.cat-product-left > div > button > span > span.label-compare-off");
    final By COMPARE_BUTTON = By.xpath("//*[@id=\"section_compare\"]/div/div/div/div/div[2]/a");


    public void setPriceRange(String lowPrice, String highPrice) {
        sendText(FILTER_PRICE_FROM, lowPrice);
        sendText(FILTER_PRICE_TO, highPrice);
        clickOnElement(FILTER_BUTTON);

    }
    public void compareElements(By firstSortingCriteria, By secondSortingCriteria){
        clickOnElement(SORTING_BUTTON);
        clickOnElement(firstSortingCriteria);
        clickOnElement(FIRST_PRODUCT);
        clickOnElement(SORTING_BUTTON);
        clickOnElement(secondSortingCriteria);
        clickOnElement(SECOND_PRODUCT);
        clickOnElement(COMPARE_BUTTON);
    }

}
