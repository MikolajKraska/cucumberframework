package com.testarmy.samples.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.logging.Level;
import java.util.logging.Logger;


public class BasePage {

    protected final Logger log = Logger.getLogger(getClass().getName());
    WebDriver driver;

    public BasePage(WebDriver driver){
        this.driver = driver;
    }

    public void clickOnElement(By element) {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(element));
            driver.findElement(element).click();
        }
        catch(org.openqa.selenium.StaleElementReferenceException ex) {
            wait.until(ExpectedConditions.visibilityOfElementLocated(element));
            driver.findElement(element).click();
            log.log(Level.WARNING, "Element is not visible. Second try " + element);
        }
        catch(org.openqa.selenium.ElementClickInterceptedException ex) {
            log.log(Level.SEVERE, "Click on the element failed " + element);
        }
    }
    public void clickOnXpathElement(String element) {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(element)));
        driver.findElement(By.xpath(element)).click();
    }
    public void clickOnCssElement(String element) {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(element)));
        driver.findElement(By.cssSelector(element)).click();
    }
    public void sendText(By element, String text) {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(element));
            driver.findElement(element).clear();
            driver.findElement(element).sendKeys(Keys.chord(Keys.CONTROL, "a"));
            driver.findElement(element).sendKeys(text);
        }
        catch (org.openqa.selenium.StaleElementReferenceException ex) {
            wait.until(ExpectedConditions.presenceOfElementLocated(element));
            driver.findElement(element).clear();
            driver.findElement(element).sendKeys(Keys.chord(Keys.CONTROL, "a"));
            driver.findElement(element).sendKeys(text);
        }
    }
    public void checkTextElementByXpath(String element, String expected){
        String value = driver.findElement(By.xpath(element)).getText();
        Assert.assertEquals(value, expected);
        System.out.println("List contain word " + value);
    }
    public void checkTextElementByCss(String element, String expected){
        String value = driver.findElement(By.cssSelector(element)).getText();
        Assert.assertEquals(value, expected);
        System.out.println("List contain word " + value);
    }


    public boolean isElementClickable(By path) {
        WebElement element = driver.findElement(path);
        return element.isDisplayed() && element.isEnabled();
    }

    public String saveText(By element){
        return driver.findElement(element).getText();
    }

    public void get(String webpage) {
        driver.get(webpage);
    }
}