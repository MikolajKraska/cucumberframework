package com.testarmy.samples.steps;

import com.testarmy.samples.pages.CompareDevicesPage;
import com.testarmy.samples.pages.MoreleDevicesPage;
import com.testarmy.samples.support.Constants;
import com.testarmy.samples.support.LazyWebDriver;
import com.testarmy.samples.MainRunnerTest;
import com.testarmy.samples.pages.MorelePage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.util.logging.Logger;

public class MoreleSteps extends MainRunnerTest {

    protected final Logger log = Logger.getLogger(getClass().getName());

    MorelePage morelePage;
    MoreleDevicesPage moreleDevicesPage;
    CompareDevicesPage compareDevicesPage;

    private final LazyWebDriver lazyWebDriver;

    public MoreleSteps(LazyWebDriver lazyWebDriver){
        this.lazyWebDriver = lazyWebDriver;
        morelePage = new MorelePage(lazyWebDriver.getDelegate());
        moreleDevicesPage = new MoreleDevicesPage(lazyWebDriver.getDelegate());
        compareDevicesPage = new CompareDevicesPage(lazyWebDriver.getDelegate());
    }

    @Given("^User navigates to morele.net website$")
    public void user_navigates_to_morele_website() throws Throwable {
        morelePage.get(Constants.MAINPAGE);
        morelePage.clickOnElement(MorelePage.COOKIE);
    }

    @And("^User checks the operation of grouping on the page$")
    public void User_checks_grouping_on_the_page() {
        morelePage.checkDropdownElements();

    }

    @When("^User selects the cheapest and the most expensive device$")
    public void user_select_cheap_and_expensive_devices() throws Throwable {
        morelePage.searchText("Samsung");
        moreleDevicesPage.setPriceRange("200", "5000");
        moreleDevicesPage.compareElements(MoreleDevicesPage.SORT_PRICE_FROM_LOW, MoreleDevicesPage.SORT_PRICE_FROM_HIGH);


    }

    @When("^User selects the most commented and the most popular devices$")
    public void user_select_popular_and_commented_devices() throws Throwable {
        morelePage.searchText("Samsung");
        moreleDevicesPage.setPriceRange("200", "5000");
        moreleDevicesPage.compareElements(MoreleDevicesPage.MOST_POPULAR, MoreleDevicesPage.COMMENTS_AMOUNT);

    }

    @Then("^User compare two devices$")
    public void user_compare_two_devices() throws Throwable {
        compareDevicesPage.compareDevicesValues(CompareDevicesPage.FIRST_PRODUCT_PRICE, CompareDevicesPage.SECOND_PRODUCT_PRICE);
        Assert.assertArrayEquals(compareDevicesPage.getProductDetails(), new String[] {"Samsung","Samsung"});
        Assert.assertTrue("Element isn't clickable", compareDevicesPage.isAddToCartClickable());

    }
}
