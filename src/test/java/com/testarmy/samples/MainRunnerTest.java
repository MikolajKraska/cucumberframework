package com.testarmy.samples;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/test/resources"},
        glue = {"com.testarmy.samples"},
        monochrome = true,
        plugin = {"pretty", "html:target/cucumber",
                "json:target/cucumber.json",
        }
)
public class MainRunnerTest {
}
