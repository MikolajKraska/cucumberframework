package com.testarmy.samples.support;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class LazyWebDriver implements org.picocontainer.Disposable{

    private WebDriver delegate;

    public WebDriver getDelegate() {
        if (delegate == null) {
            System.setProperty("webdriver.chrome.driver", "C:\\selenium\\chromedriver_win32\\chromedriver.exe");
            this.delegate = new ChromeDriver();
            this.delegate.manage().window().maximize();
            this.delegate.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
            this.delegate.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
        }
        return delegate;
    }

    @Override
    public void dispose() {
        this.delegate.manage().deleteAllCookies();
        this.delegate.quit();
        this.delegate = null;
    }

}
